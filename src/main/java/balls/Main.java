package balls;

import balls.listeners.MouseAdapterImpl;
import balls.models.Ball;
import balls.services.Service;
import balls.view.Display;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class Main {
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 750;
    public static final int MAX_R = WIDTH / 8;
    public static final int MIN_R = 25;
    public static final long UPDATE_RATE = 1000 / 120;
    public static final long TIME_STEP = 1000 / 60;
    public static final double HEALTH = 1000d;
    public static final double DAMAGE = 50d;
    public static final String TITLE = "Balls";

    public static void main(String[] args) {

        JFrame frame = new JFrame(TITLE);
        Canvas canvas = new Canvas();
        Dimension windowSize = new Dimension(WIDTH, HEIGHT);
        BufferedImage buffer = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_ARGB);
        int clearColor = 0xFFFFFFC8;
        List<Ball> balls = new CopyOnWriteArrayList<>();
        Random random = new Random();
        Service service = new Service(balls, random);
        Thread serviceThread = new Thread(service);
        serviceThread.start();
        MouseAdapterImpl m = new MouseAdapterImpl(service);
        Display display = new Display(frame, canvas, windowSize, buffer, clearColor, balls, m);

        display.start();

        Timer t = new Timer((int) UPDATE_RATE, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display.clear();
                display.render();
                display.swapBuffers();
            }
        });
        t.setRepeats(true);
        t.start();
    }
}
