package balls.models;

import java.awt.*;

public class Ball {
    private static int generalId = 0;
    private int id;
    private double x;
    private double y;
    private double r;
    private double dX;
    private double dY;
    private double health;
    private Color color;

    public Ball(double x, double y, double r, double dX, double dY, Color color, double health) {
        id = generalId++;
        this.x = x;
        this.y = y;
        this.r = r;
        this.dX = dX;
        this.dY = dY;
        this.color = color;
        this.health = health;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return id == ball.id;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getdX() {
        return dX;
    }

    public void setdX(double dX) {
        this.dX = dX;
    }

    public double getdY() {
        return dY;
    }

    public void setdY(double dY) {
        this.dY = dY;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public double getHealth() {
        return health;
    }

    public void setHealth(double health) {
        this.health = health;
    }
}
