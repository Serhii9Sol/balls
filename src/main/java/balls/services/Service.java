package balls.services;

import balls.Main;
import balls.models.Ball;
import java.awt.*;
import java.util.List;
import java.util.Random;

public class Service implements Runnable {
    private List<Ball> balls;
    private Random random;

    public Service(List<Ball> balls, Random random) {
        this.balls = balls;
        this.random = random;
    }

    @Override
    public void run() {
        long start;
        long delta;
        while (true) {
            start = System.currentTimeMillis();
            for (Ball b: balls) {
                while (checkCollision() & checkBoarders(b)) {

                }
                checkHealthStatus(b);
                moveBall(b);
            }
            delta = System.currentTimeMillis() - start;
            if (delta < Main.TIME_STEP) {
                try {
                    Thread.sleep(Main.TIME_STEP - delta);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void createRandomBall(int x, int y) {
        boolean isPossible = true;

        double radius = random.nextInt(Main.MAX_R - Main.MIN_R) + Main.MIN_R;
        double dx = random.nextDouble() * 4d - 2;
        double dy = random.nextDouble() * 4d - 2;
        double x1 = x - radius / 2;
        double y1 = y - radius / 2;
        Color color = new Color(random.nextInt(Integer.MAX_VALUE));
        Ball newBall = new Ball(x1, y1, radius, dx, dy, color, Main.HEALTH);
        Ball newBallOnNextStep = new Ball(x1 + dx, y1 + dy, radius, dx, dy, color, Main.HEALTH);

        for (Ball b: balls) {
            isPossible = (getDistance(newBall, b) > 1.05d) & (getDistance(newBallOnNextStep, b) > 1.05d);
        }
        if (isPossible & !checkBoarders(newBall)) {
            balls.add(newBall);
        }
    }

    private void moveBall(Ball b) {
        b.setX(b.getX() + b.getdX());
        b.setY(b.getY() + b.getdY());
    }

    private boolean checkBoarders(Ball b) {
        double nextX = b.getX() + b.getdX();
        double nextY = b.getY() + b.getdY();
        double r = b.getR();
        if (nextX <= 0 || (nextX + r >= Main.WIDTH)) {
            b.setdX(b.getdX() * -1d);
            return true;
        } else if (nextY <= 0 || (nextY + r >= Main.HEIGHT)) {
            b.setdY(b.getdY() * -1d);
            return true;
        }
        return false;
    }

    private boolean checkCollision() {
        if (balls.size() > 1) {
            double distance;
            for (int i = 0; i < balls.size() - 1; i++) {
                for (int j = i + 1; j < balls.size(); j++) {
                    distance = getDistance(balls.get(i), balls.get(j));
                    if (checkDistance(balls.get(i), balls.get(j))) {
                        doCollision(balls.get(i), balls.get(j), distance);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean checkDistance(Ball b1, Ball b2) {
        double r1 = b1.getR() / 2;
        double r2 = b2.getR() / 2;
        double nextX1 = b1.getX() + b1.getdX() + r1;
        double nextY1 = b1.getY() + b1.getdY() + r1;
        double nextX2 = b2.getX() + b2.getdX() + r2;
        double nextY2 = b2.getY() + b2.getdY() + r2;
        return Math.sqrt(Math.pow((nextX2 - nextX1), 2) + Math.pow((nextY2 - nextY1), 2)) <= (r1 + r2);
    }

    private boolean doCollision(Ball b1, Ball b2, double distance) {
        double r1 = b1.getR() / 2;
        double r2 = b2.getR() / 2;
        double m1 = 3.14 * (r1) * (r1);
        double m2 = 3.14 * (r2) * (r2);

        double ex1 = m1 * b1.getdX() * b1.getdX() / 2;
        double ey1 = m1 * b1.getdY() * b1.getdY() / 2;
        double ex2 = m2 * b2.getdX() * b2.getdX() / 2;
        double ey2 = m2 * b2.getdY() * b2.getdY() / 2;
        double eTotal = ex1 + ey1 + ex2 + ey2;
        double b1Damage = (1d - ((ex1 + ey1) / eTotal)) * Main.DAMAGE;
        double b2Damage = (1d - ((ex2 + ey2) / eTotal)) * Main.DAMAGE;

        double newDx1 = (2 * m2 * b2.getdX() + (m1 - m2) * b1.getdX()) / (m1 + m2);
        double newDy1 = (2 * m2 * b2.getdY() + (m1 - m2) * b1.getdY()) / (m1 + m2);
        double newDx2 = (2 * m1 * b1.getdX() + (m2 - m1) * b2.getdX()) / (m1 + m2);
        double newDy2 = (2 * m1 * b1.getdY() + (m2 - m1) * b2.getdY()) / (m1 + m2);

        b1.setHealth(b1.getHealth() - b1Damage);
        b2.setHealth(b2.getHealth() - b2Damage);
        b1.setdX(newDx1 / distance);
        b2.setdX(newDx2 / distance);
        b1.setdY(newDy1 / distance);
        b2.setdY(newDy2 / distance);

        return true;
    }

    private double getDistance(Ball b1, Ball b2) {
        return Math.sqrt(Math.pow((b1.getX() - b2.getX()), 2) + Math.pow((b1.getY() - b2.getY()), 2)) /
                (b1.getR() / 2 + b2.getR() / 2);
    }

    private void checkHealthStatus(Ball b) {
        if (b.getHealth() < 0d) {
            balls.remove(b);
        }
    }
}
