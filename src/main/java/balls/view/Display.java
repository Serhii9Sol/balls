package balls.view;

import balls.Main;
import balls.models.Ball;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Arrays;
import java.util.List;

public class Display {
    private JFrame window;
    private Canvas canvas;
    private Dimension wSize;
    private BufferedImage buffer;
    private int[] bufferData;
    private Graphics bufferGraphics;
    private int clearColor;
    private List<Ball> balls;
    private MouseAdapter mouseAdapter;
    private Color cColor;

    public Display(JFrame window, Canvas canvas, Dimension wSize, BufferedImage buffer, int clearColor,
                   List<Ball> balls, MouseAdapter mouseAdapter) {
        this.window = window;
        this.canvas = canvas;
        this.wSize = wSize;
        this.buffer = buffer;
        this.clearColor = clearColor;
        this.balls = balls;
        this.mouseAdapter = mouseAdapter;
        this.cColor = new Color(clearColor);
    }

    public void start() {
        canvas.setPreferredSize(wSize);
        window.setResizable(false);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.getContentPane().add(canvas);
        window.pack();
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        bufferData = ((DataBufferInt) buffer.getRaster().getDataBuffer()).getData();
        bufferGraphics = buffer.getGraphics();
        ((Graphics2D) bufferGraphics).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        canvas.addMouseListener(mouseAdapter);
    }

    public void clear() {
        Arrays.fill(bufferData, clearColor);
    }

    public void render() {
        int x, x1, y, y1, d, d1;
        for (Ball b: balls) {
            x = (int) b.getX();
            y = (int) b.getY();
            d = (int) b.getR();
            d1 = (int) (b.getR() * (1d - (b.getHealth() / Main.HEALTH)));
            x1 = x + d / 2 - d1 / 2;
            y1 = y + d / 2 - d1 / 2;

            bufferGraphics.setColor(b.getColor());
            bufferGraphics.fillOval(x, y, d, d);
            bufferGraphics.setColor(cColor);
            bufferGraphics.fillOval(x1, y1, d1, d1);
        }
    }

    public void swapBuffers() {
        Graphics g = canvas.getGraphics();
        g.drawImage(buffer, 0, 0, null);
    }
}
